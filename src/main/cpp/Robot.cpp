/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "Robot.h"

#include <iostream>

#include <frc/smartdashboard/SmartDashboard.h>

void Robot::RobotInit() {
  m_chooser.SetDefaultOption(kAutoNameDefault, kAutoNameDefault);
  m_chooser.AddOption(kAutoNameCustom, kAutoNameCustom);
  frc::SmartDashboard::PutData("Auto Modes", &m_chooser);


  clawEncoder1.Reset();
  elevatorEncoder1.Reset();

  Compressor.Start();

  elevatorShifter.Set(0);
  clawPiston.Set(0);
  frontLegLatch.Set(0);
  rearLagLatch.Set(0);

  elevatorControl.SetOutputRange(-0.9, 0.00);
  elevatorControl.Enable();

  clawControl.SetOutputRange(-0.25, .80);
  clawControl.Enable();

  humanFeedTimer.Start();
  
}
void Robot::InitTables(){
  auto inst = nt::NetworkTableInstance::GetDefault();
  auto table = inst.GetTable("SmartDashboard");
  // init smartdashboard indicators here


  table = inst.GetTable("limelight");
  // limelight tables here
  e_tx = table->GetEntry("tx");
  e_ty = table->GetEntry("ty");
  e_ts = table->GetEntry("ts");
  e_tv = table->GetEntry("tv");
  ledMode = table->GetEntry("ledMode");
}
/**
 * This function is called every robot packet, no matter the mode. Use
 * this for items like diagnostics that you want ran during disabled,
 * autonomous, teleoperated and test.
 *
 * <p> This runs after the mode specific periodic functions, but before
 * LiveWindow and SmartDashboard integrated updating.
 */
void Robot::RobotPeriodic() {
   //DONT USE THIS

   //std::cout << 
  //"Elevator Encoder:" << elevatorEncoder1.Get() <<
  //"  Claw Encoder:" << clawEncoder1.Get() << 
 // "  Ball Sensor:" << ballSensor1.Get() <<
 // std::endl;
}

/**
 * This autonomous (along with the chooser code above) shows how to select
 * between different autonomous modes using the dashboard. The sendable chooser
 * code works with the Java SmartDashboard. If you prefer the LabVIEW Dashboard,
 * remove all of the chooser code and uncomment the GetString line to get the
 * auto name from the text box below the Gyro.
 *
 * You can add additional auto modes by adding additional comparisons to the
 * if-else structure below with additional strings. If using the SendableChooser
 * make sure to add them to the chooser code above as well.
 */
void Robot::AutonomousInit() {
  m_autoSelected = m_chooser.GetSelected();
  // m_autoSelected = SmartDashboard::GetString("Auto Selector",
  //     kAutoNameDefault);
  if (m_autoSelected == kAutoNameCustom) {
    // Custom Auto goes here
  } else {
    // Default Auto goes here
  }
}

void Robot::AutonomousPeriodic() {
  if (m_autoSelected == kAutoNameCustom) {
    // Custom Auto goes here
  } else {
    // Default Auto goes here
  }
}

void Robot::TeleopInit() {

}

void Robot::TeleopPeriodic() {

//Drive Code
  if(stick.GetRawAxis(1)>0.02 || stick.GetRawAxis(1)<-0.02)
  {
    leftJoystick = stick.GetRawAxis(1);
  }
  else
  {
    leftJoystick = 0.0;
  }

  if(stick.GetRawAxis(5)>0.02 || stick.GetRawAxis(5)<-0.02)
  {
    rightJoystick = stick.GetRawAxis(5);
  }
  else
  {
    rightJoystick = 0.0;
  }
  TankDrive(leftJoystick, rightJoystick);

  //CarrageMotor1.Set(stick.GetRawAx

  //elevatorMotor1.Set(stick.GetRawAxis(5));
  //elevatorMotor2.Set(stick.GetRawAxis(5));
  //elevatorMotor3.Set(stick.GetRawAxis(5));

  //std::cout << "Alive!" << std::endl;

//HAtch Pistons
  if(stick.GetRawButton(2) || stick.GetRawButton(6)) 
  {
    clawPiston.Set(1); 
  }
  else
  {
    clawPiston.Set(0); 
  }

//Intake Motors
  if(stick.GetRawButton(3) ||  stick.GetRawAxis(3) > 0.75) //intake
  {
    IntakeMotorTop.Set(0.9); 
    IntakeMotorBottom.Set(-1.0);
  }
  else if(stick.GetRawButton(4) && clawEncoder1.Get() > 900) //outtake
  {
    IntakeMotorTop.Set(-0.9);  
    IntakeMotorBottom.Set(1.0);
  }
  else
  {
    if(ballSensor1.Get()) //if ball, run in slowly
    {
      IntakeMotorTop.Set(0.25);
      IntakeMotorBottom.Set(0.0);
    }
    else
    {
      IntakeMotorTop.Set(0.0);
      IntakeMotorBottom.Set(0.0);
    }
  }

//Elevator Control
  if(stick.GetRawAxis(2) > 0.75)  //level 3
  {
    elevatorControl.SetSetpoint(1100);  //1100
  }
  else if(stick.GetPOV() == 270) ///level 2
  {
    elevatorControl.SetSetpoint(600);  //600
  }
  else if(stick.GetRawButton(5) || stick.GetRawButton(6) || stick.GetRawAxis(3) > 0.75) //intake mode and level 1
  {
    elevatorControl.SetSetpoint(10); 
  }
  else //Stowed
  {
    elevatorControl.SetSetpoint(100); 
  }


//Claw Control
 if(stick.GetRawButton(6) || stick.GetRawButton(1))  //intake mode
  {
    if(elevatorEncoder1.Get() < 75)
    {
      clawControl.SetSetpoint(50); 
    }
    else
    {
      clawControl.SetSetpoint(500); 
    }
  }
  else if((stick.GetRawButton(5) || stick.GetPOV() == 270 || stick.GetRawAxis(2) > 0.75) && ballSensor1.Get()) //has ball, keep claw up
  {
    clawControl.SetSetpoint(1000); 
  }
  else if(stick.GetRawAxis(2) > 0.75) //level 3 hatch
  {
    if(elevatorEncoder1.Get() > 550) //keep angled up util withn 100 of setpoint
    {
      clawControl.SetSetpoint(35); 
    }
    else
    {
      clawControl.SetSetpoint(1000); 
    }
  }
  else if(stick.GetPOV() == 270) //level 2 //level 3 hatch
  {
    if(elevatorEncoder1.Get() > 150) //keep angled up util withn 100 of setpoint
    {
      clawControl.SetSetpoint(35); 
    }
    else
    {
      clawControl.SetSetpoint(1000); 
    }
  }
   else if(stick.GetRawButton(6) || stick.GetRawAxis(3) > 0.75 || stick.GetRawButton(5)) //intake mode and level 1
  {
     if(elevatorEncoder1.Get() < 75) 
    {
      clawControl.SetSetpoint(17); 
    }
    else
    {
      clawControl.SetSetpoint(500); 
    }
  }
  else
  {
    if(humanFeedTimer.Get() > 1.0) //wait one secound befor raising fro
    {
      clawControl.SetSetpoint(1000); 
      humanFeedTimer.Stop();
    }
  }

  if(stick.GetRawButton(6)) //reset timer in human feeder mode only.
  {
    humanFeedTimer.Start();
    humanFeedTimer.Reset();
  }

  //set elevatorMotor2 and elevatorMotor3 to same speed as eleatorMotor1
  elevatorMotor2.Set(elevatorMotor1.Get());
  elevatorMotor3.Set(elevatorMotor1.Get());
}

void Robot::TestPeriodic() {}

//Tank Drive Wrapper 
void Robot::TankDrive(double left, double right){
  leftSpark1.Set(right);
  leftSpark2.Set(right);
  leftSpark3.Set(right);

  rightSpark1.Set(-left);
  rightSpark2.Set(-left);
  rightSpark3.Set(-left);

}

#ifndef RUNNING_FRC_TESTS
int main() { return frc::StartRobot<Robot>(); }
#endif
