/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#pragma once

#include <string>

#include <frc/TimedRobot.h>
#include <frc/smartdashboard/SendableChooser.h>
#include "rev/CANSparkMax.h"
#include "frc/WPILib.h"
#include "ctre/phoenix/motorcontrol/can/WPI_VictorSPX.h"
#include "networktables/NetworkTable.h"
#include "networktables/NetworkTableInstance.h"

class Robot : public frc::TimedRobot {
 public:
  void RobotInit() override;
  void RobotPeriodic() override;
  void AutonomousInit() override;
  void AutonomousPeriodic() override;
  void TeleopInit() override;
  void TeleopPeriodic() override;
  void TestPeriodic() override;

  void InitTables();
  void TankDrive(double left, double right);

  frc::Joystick stick {0};
  nt::NetworkTableEntry e_tx;
  nt::NetworkTableEntry e_ty;
  nt::NetworkTableEntry e_ts;
  nt::NetworkTableEntry e_tv;
  nt::NetworkTableEntry ledMode;

  rev::CANSparkMax leftSpark1 {8, rev::CANSparkMax::MotorType::kBrushless};
  rev::CANSparkMax leftSpark2 {9, rev::CANSparkMax::MotorType::kBrushless};
  rev::CANSparkMax leftSpark3 {10, rev::CANSparkMax::MotorType::kBrushless};

  rev::CANSparkMax rightSpark1 {11, rev::CANSparkMax::MotorType::kBrushless};
  rev::CANSparkMax rightSpark2 {12, rev::CANSparkMax::MotorType::kBrushless};
  rev::CANSparkMax rightSpark3 {13, rev::CANSparkMax::MotorType::kBrushless};  

  ctre::phoenix::motorcontrol::can::WPI_VictorSPX elevatorMotor1 {0};
  ctre::phoenix::motorcontrol::can::WPI_VictorSPX elevatorMotor2 {1};
  ctre::phoenix::motorcontrol::can::WPI_VictorSPX elevatorMotor3 {2};

  ctre::phoenix::motorcontrol::can::WPI_VictorSPX IntakeMotorTop {4};
  ctre::phoenix::motorcontrol::can::WPI_VictorSPX IntakeMotorBottom {3};
  ctre::phoenix::motorcontrol::can::WPI_VictorSPX CarrageMotor1 {5};

  frc::Encoder clawEncoder1 {9, 8};
  frc::Encoder elevatorEncoder1 {7, 6};

  frc::Compressor Compressor {6};

  frc::Solenoid elevatorShifter{6, 0};
  frc::Solenoid clawPiston{6,1};
  frc::Solenoid frontLegLatch{6,2};
  frc::Solenoid rearLagLatch{6,3};

  frc::PIDController elevatorControl{-0.0040, 0, 0, elevatorEncoder1, elevatorMotor1, 0.02};
  frc::PIDController clawControl{0.0035, 0, 0, clawEncoder1, CarrageMotor1, 0.05};

  frc::Timer humanFeedTimer{};

  frc::DigitalInput ballSensor1{0};

  double leftJoystick = 0;
  double rightJoystick = 0;

 private:
  frc::SendableChooser<std::string> m_chooser;
  const std::string kAutoNameDefault = "Default";
  const std::string kAutoNameCustom = "My Auto";
  std::string m_autoSelected;
};
